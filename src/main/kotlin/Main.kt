import kotlinx.browser.document
import kotlinx.browser.window
import react.*
import react.dom.html.ReactHTML.div
import react.dom.html.ReactHTML.h1
import react.dom.html.ReactHTML.h4
import react.dom.html.ReactHTML.li
import react.dom.html.ReactHTML.option
import react.dom.html.ReactHTML.select
import react.dom.html.ReactHTML.ul
import react.dom.render

fun main() {
    window.onload = {
        render(document.getElementById("root")!!) {
            child(Page)
        }
    }
}

val Page = fc<Props> {

    h1 {
        +"Welcome to Kotlin/JS + React!"
    }

    ul {
        h4 { +"Kotlin/JS" }
        li { +"Project configuration" }
        li { +"HTML/CSS/Logic in Kotlin" }
        li { +"Document/Console/Window/LocalStorage etc." }
        li { +"Plain JS" }
        li { +"Exceptions" }

        h4 { +"React" }
        li {
            +"Reusable components"
            ul {
                li { +"Class vs Functional" }
                li { +"Properties, State & Effects" }
            }
        }
    }
}
